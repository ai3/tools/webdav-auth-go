package main

import (
	"context"
	"errors"
	"fmt"

	ldaputil "git.autistici.org/ai3/go-common/ldap"
	"github.com/go-ldap/ldap/v3"
)

func usernameFromDN(dnStr string) (string, error) {
	dn, err := ldap.ParseDN(dnStr)
	if err != nil {
		return "", err
	}

	if len(dn.RDNs) < 1 || len(dn.RDNs[0].Attributes) < 1 {
		return "", errors.New("malformed DN")
	}

	attr := dn.RDNs[0].Attributes[0]
	if attr.Type != "ftpname" {
		return "", fmt.Errorf("unexpected RDN type '%s'", attr.Type)
	}
	return attr.Value, nil
}

type ldapClient struct {
	baseDN string
	pool   *ldaputil.ConnectionPool
}

func newLDAPClient(uri, baseDN, bindDN, bindPw string) (*ldapClient, error) {
	pool, err := ldaputil.NewConnectionPool(uri, bindDN, bindPw, 5)
	if err != nil {
		return nil, err
	}
	return &ldapClient{
		baseDN: baseDN,
		pool:   pool,
	}, nil
}

var errNoResults = errors.New("no results")

func (l *ldapClient) checkUidAndHost(ctx context.Context, shardID string, uid int) error {
	req := ldap.NewSearchRequest(
		l.baseDN,
		ldap.ScopeWholeSubtree,
		ldap.NeverDerefAliases,
		0,
		0,
		false,
		fmt.Sprintf("(&(objectClass=ftpAccount)(status=active)(host=%s)(uidNumber=%d))", ldap.EscapeFilter(shardID), uid),
		[]string{"dn"},
		nil,
	)
	result, err := l.pool.Search(ctx, req)
	if err != nil {
		return err
	}

	// Just check that we got a result, don't care about the value.
	if len(result.Entries) < 1 {
		return errNoResults
	}
	return nil
}

func (l *ldapClient) getDAVAccountsByUid(ctx context.Context, shardID string, uid int) ([]*account, error) {
	req := ldap.NewSearchRequest(
		l.baseDN,
		ldap.ScopeWholeSubtree,
		ldap.NeverDerefAliases,
		0,
		0,
		false,
		fmt.Sprintf("(&(objectClass=ftpAccount)(status=active)(host=%s)(uidNumber=%d))", ldap.EscapeFilter(shardID), uid),
		[]string{"ftpname", "homeDirectory"},
		nil,
	)
	result, err := l.pool.Search(ctx, req)
	if err != nil {
		return nil, err
	}

	accounts := make([]*account, 0, len(result.Entries))
	for _, entry := range result.Entries {
		accounts = append(accounts, &account{
			DN:   entry.DN,
			Name: entry.GetAttributeValue("ftpname"),
			Path: entry.GetAttributeValue("homeDirectory"),
		})
	}
	return accounts, nil
}
