package lineproto

import (
	"bufio"
	"net"
)

type Reader struct {
	r *bufio.Reader
}

func (r *Reader) ReadLine() ([]byte, error) {
	var line []byte
	for {
		l, more, err := r.r.ReadLine()
		if err != nil {
			return nil, err
		}
		// Avoid the copy if the first call produced a full line.
		if line == nil && !more {
			return l, nil
		}
		line = append(line, l...)
		if !more {
			break
		}
	}
	return line, nil
}

type Writer struct {
	w *bufio.Writer
}

var crlf = []byte("\r\n")

func (w *Writer) WriteLine(args ...[]byte) error {
	for _, arg := range args {
		_, err := w.w.Write(arg)
		if err != nil {
			return err
		}
	}
	_, err := w.w.Write(crlf)
	if err != nil {
		return err
	}
	return w.w.Flush()
}

type Conn struct {
	net.Conn
	*Reader
	*Writer

	ServerName string
}

func NewConn(c net.Conn, name string) *Conn {
	return &Conn{
		Conn:       c,
		Reader:     &Reader{r: bufio.NewReader(c)},
		Writer:     &Writer{w: bufio.NewWriter(c)},
		ServerName: name,
	}
}

func (c *Conn) Close() error {
	return c.Conn.Close()
}
