package lineproto

import (
	"container/list"
	"net"
	"sync"
	"sync/atomic"
)

type Handler interface {
	ServeConnection(c *Conn)
}

type Server struct {
	Name string

	l net.Listener
	h Handler

	// Keep track of active connections so we can shut them down
	// on Close.
	closing atomic.Value
	wg      sync.WaitGroup
	connMx  sync.Mutex
	conns   list.List
}

func NewServer(name string, l net.Listener, h Handler) *Server {
	s := &Server{
		Name: name,
		l:    l,
		h:    h,
	}
	s.closing.Store(false)
	return s
}

// Close the socket listener and release all associated resources.
// Waits for active connections to terminate before returning.
func (s *Server) Close() {
	s.closing.Store(true)

	// Close the listener to stop incoming connections.
	s.l.Close() // nolint

	// Close all active connections (this will return an error to
	// the client if the connection is not idle).
	s.connMx.Lock()
	for el := s.conns.Front(); el != nil; el = el.Next() {
		el.Value.(net.Conn).Close()
	}
	s.connMx.Unlock()

	s.wg.Wait()
}

func (s *Server) isClosing() bool {
	return s.closing.Load().(bool)
}

// Serve connections.
func (s *Server) Serve() error {
	for {
		conn, err := s.l.Accept()
		if err != nil {
			if s.isClosing() {
				return nil
			}
			return err
		}

		s.wg.Add(1)

		s.connMx.Lock()
		connEl := s.conns.PushBack(conn)
		s.connMx.Unlock()

		go func() {
			s.h.ServeConnection(NewConn(conn, s.Name))
			conn.Close() // nolint
			if !s.isClosing() {
				s.connMx.Lock()
				s.conns.Remove(connEl)
				s.connMx.Unlock()
			}
			s.wg.Done()
		}()
	}
}
