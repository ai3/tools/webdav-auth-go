package main

import (
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"net"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"syscall"
	"time"

	"git.autistici.org/ai3/go-common/unix"
	"git.autistici.org/id/auth"
	authclient "git.autistici.org/id/auth/client"
)

var (
	authSocketPath = flag.String("auth-socket", authclient.DefaultSocketPath, "authentication socket `path`")
	authService    = flag.String("auth-service", "dav", "service for authentication")
	socketPath     = flag.String("socket", "/run/authdav/auth", "UNIX socket `path`")
	socketPerm     = flag.String("socket-perm", "777", "permission bits for socket (octal)")
	shardID        = flag.String("shard-id", "", "shard `id`")

	ldapURI    = flag.String("ldap-uri", "ldapi:///var/run/ldap/ldapi", "LDAP `uri`")
	ldapBindDN = flag.String("ldap-bind-dn", "", "LDAP bind DN")
	ldapBindPw fileFlag
	ldapBaseDN = flag.String("ldap-base-dn", "ou=People,dc=investici,dc=org,o=Anarchy", "base DN for LDAP searches")
)

func init() {
	flag.Var(&ldapBindPw, "ldap-bind-pwfile", "file with LDAP bind password")
}

type fileFlag string

func (s fileFlag) String() string { return string(s) }

func (s *fileFlag) Set(value string) error {
	data, err := os.ReadFile(value)
	if err != nil {
		return err
	}
	*s = fileFlag(strings.TrimSpace(string(data)))
	return nil
}

var requestTimeout = 30 * time.Second

type account struct {
	Name string `json:"ftpname"`
	Path string `json:"home"`
	DN   string `json:"dn"`
}

type authServer struct {
	ldap *ldapClient
	auth authclient.Client
}

func newAuthServer() (*authServer, error) {
	l, err := newLDAPClient(*ldapURI, *ldapBaseDN, *ldapBindDN, ldapBindPw.String())
	if err != nil {
		return nil, err
	}
	return &authServer{
		ldap: l,
		auth: authclient.New(*authSocketPath),
	}, nil
}

func (s *authServer) handleGetAccounts(ctx context.Context, uid int) (map[string]*account, error) {
	accounts, err := s.ldap.getDAVAccountsByUid(ctx, *shardID, uid)
	if err != nil {
		return nil, err
	}

	m := make(map[string]*account)
	for _, acct := range accounts {
		realm := fmt.Sprintf("/dav/%s", acct.Name)
		m[realm] = acct
	}
	return m, nil
}

const (
	authResultError = "error"
	authResultOk    = "ok"
)

func (s *authServer) handleAuthenticate(ctx context.Context, uid int, dn, password string) (string, error) {
	if err := s.ldap.checkUidAndHost(ctx, *shardID, uid); err != nil {
		return authResultError, err
	}

	username, err := usernameFromDN(dn)
	if err != nil {
		return authResultError, fmt.Errorf("couldn't parse username from '%s': %w", dn, err)
	}

	resp, err := s.auth.Authenticate(ctx, &auth.Request{
		Service:  *authService,
		Username: username,
		Password: []byte(password),
	})
	if err != nil {
		return authResultError, fmt.Errorf("auth protocol error: %w", err)
	}
	if resp.Status == auth.StatusOK {
		return authResultOk, nil
	}
	return authResultError, resp.Status
}

type request struct {
	Type     string `json:"type"`
	DN       string `json:"dn"`
	Password string `json:"password"`
}

func (s *authServer) ServeConnection(nc net.Conn) {
	conn, ok := nc.(*net.UnixConn)
	if !ok {
		// Build issue.
		panic("not a net.UnixConn")
	}
	defer conn.Close()

	f, err := conn.File()
	if err != nil {
		log.Printf("can't get conn.File: %v", err)
		return
	}
	defer f.Close()

	cred, err := syscall.GetsockoptUcred(int(f.Fd()), syscall.SOL_SOCKET, syscall.SO_PEERCRED)
	if err != nil {
		log.Printf("can't get peer credentials: %v", err)
		return
	}
	uid := int(cred.Uid)

	var req request
	if err := json.NewDecoder(f).Decode(&req); err != nil {
		log.Printf("error reading request (uid=%d): %v", uid, err)
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), requestTimeout)
	defer cancel()

	var reqStr string
	var resp any
	switch req.Type {
	case "get_accounts":
		reqStr = "get_accounts()"
		resp, err = s.handleGetAccounts(ctx, uid)
	case "auth":
		reqStr = fmt.Sprintf("auth(%s)", req.DN)
		resp, err = s.handleAuthenticate(ctx, uid, req.DN, req.Password)
	default:
		log.Printf("unknown request type \"%s\" (uid=%d)", req.Type, uid)
		return
	}

	if err != nil {
		log.Printf("error handling request (uid=%d, %+v): %v", uid, req, err)
		return
	}

	log.Printf("request from uid=%d: %s -> %v", uid, reqStr, resp)
	if err := json.NewEncoder(f).Encode(resp); err != nil {
		log.Printf("error writing response (uid=%d): %v", uid, err)
	}
}

func main() {
	log.SetFlags(0)
	flag.Parse()

	as, err := newAuthServer()
	if err != nil {
		log.Fatal(err)
	}

	srv, err := unix.NewUNIXSocketServer(*socketPath, as)
	if err != nil {
		log.Fatalf("can't start server: %v", err)
	}

	if perm, err := strconv.ParseInt(*socketPerm, 8, 64); err == nil {
		if err := os.Chmod(*socketPath, os.FileMode(perm)); err != nil {
			log.Fatalf("can't set permissions on socket: %v", err)
		}
	}

	sigCh := make(chan os.Signal, 1)
	go func() {
		<-sigCh
		log.Printf("terminating")
		srv.Close()
	}()
	signal.Notify(sigCh, syscall.SIGINT, syscall.SIGTERM)

	log.Printf("starting")
	if err := srv.Serve(); err != nil {
		log.Fatal(err)
	}
}
